﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace AutoLotDAL.Models.MetaData
{
    public class InventoryMetaData
    {
        [Display(Name = "Pet Name")]
        public string PetName;

        [Display(Name = "Maakke")]
        [StringLength(20, ErrorMessage = "Please enter a value less than 20 characters long.")]
        public string Make;
    }
}

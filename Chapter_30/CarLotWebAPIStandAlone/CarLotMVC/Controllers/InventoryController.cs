﻿//using System;
//using System.Data.Entity.Infrastructure;
//using System.Net;
//using System.Web.Mvc;
//using AutoLotDAL.EF;
//using AutoLotDAL.Models;
//using AutoLotDAL.Repos;

using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using AutoLotDAL.Models;
using Newtonsoft.Json;
using System.Text;
using System.Web.Mvc;
using System.Net;
using System;

namespace CarLotMVC.Controllers
{
    public class InventoryController : Controller
    {
//        private readonly InventoryRepo _repo = new InventoryRepo();
        private string _baseUrl = "http://localhost:60710/api/Inventory";

        // GET: Inventory
        //public ActionResult Index()
        //{
        //    return View(_repo.GetAll());
        //    //            return View("Foo", _repo.GetAll());     // view named Foo.cshtml
        //}

        //public ActionResult PIndex()
        //{
        //    return PartialView(_repo.GetAll());
        //}
        public async Task<ActionResult> Index()
        {
            var client = new HttpClient();
            var response = await client.GetAsync(_baseUrl);
            if (response.IsSuccessStatusCode)
            {
                var items = JsonConvert.DeserializeObject<List<Inventory>>(await response.Content.ReadAsStringAsync());
                return View(items);
            }
            return HttpNotFound();
        }

        // GET: Inventory/Details/5
        //public ActionResult Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);  // 400
        //    }
        //    Inventory inventory = _repo.GetOne(id);
        //    if (inventory == null)
        //    {
        //        return HttpNotFound();  // 404
        //    }
        //    return View(inventory);
        //}
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = new HttpClient();
            var response = await client.GetAsync($"{_baseUrl}/{id.Value}");
            if (response.IsSuccessStatusCode)
            {
                var inventory = JsonConvert.DeserializeObject<Inventory>(
                  await response.Content.ReadAsStringAsync());
                return View(inventory);
            }
            return HttpNotFound();
        }

        // GET: Inventory/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Inventory/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //        [HttpPost]
        //        [ValidateAntiForgeryToken]
        //        public ActionResult Create([Bind(Include = "Make,Color,PetName")] Inventory inventory)
        //        {
        //            if (!ModelState.IsValid) return View(inventory);
        //            try
        //            {
        //                _repo.Add(inventory);
        //            }
        //            catch (Exception ex)
        //            {
        ////                ModelState.AddModelError("Name", "Name is required");           // error for a specific property.
        //                ModelState.AddModelError(string.Empty, $@"Unable to create record: {ex.Message}");     // error for entire model.
        //                return View(inventory);
        //            }
        //            return RedirectToAction("Index");
        //        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Make,Color,PetName")] Inventory inventory)
        {
            if (!ModelState.IsValid) return View(inventory);
            try
            {
                var client = new HttpClient();
                string json = JsonConvert.SerializeObject(inventory);
                var response = await client.PostAsync(_baseUrl,
                        new StringContent(json, Encoding.UTF8, "application/json"));
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, $"Unable to create record: {ex.Message}");
            }
            return View(inventory);
        }

        // GET: Inventory/Edit/5
        //public ActionResult Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Inventory inventory = _repo.GetOne(id);
        //    if (inventory == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(inventory);
        //}
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = new HttpClient();
            var response = await client.GetAsync($"{_baseUrl}/{id.Value}");
            if (response.IsSuccessStatusCode)
            {
                var inventory = JsonConvert.DeserializeObject<Inventory>(
                   await response.Content.ReadAsStringAsync());
                return View(inventory);
            }
            return new HttpNotFoundResult();
        }

        // POST: Inventory/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "Id,Make,Color,PetName,Timestamp")] Inventory inventory)
        //{
        //    if (!ModelState.IsValid) return View(inventory);
        //    try
        //    {
        //        _repo.Save(inventory);
        //    }
        //    catch (DbUpdateConcurrencyException ex)
        //    {
        //        ModelState.AddModelError(string.Empty,
        //            $@"Unable to save the record. Another user has updated it. {ex.Message}");
        //        return View(inventory);
        //    }
        //    catch (Exception ex)
        //    {
        //        ModelState.AddModelError(string.Empty, $@"Unable to save the record. {ex.Message}");
        //        return View(inventory);
        //    }
        //    return RedirectToAction("Index");
        //}
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Make,Color,PetName,Timestamp")] Inventory inventory)
        {
            if (!ModelState.IsValid) return View(inventory);
            var client = new HttpClient();
            string json = JsonConvert.SerializeObject(inventory);
            var response = await client.PutAsync($"{_baseUrl}/{inventory.Id}",
              new StringContent(json, Encoding.UTF8, "application/json"));
            if (response.IsSuccessStatusCode)
            {
                return RedirectToAction("Index");
            }
            return View(inventory);
        }

        // GET: Inventory/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Inventory inventory = _repo.GetOne(id);
        //    if (inventory == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(inventory);
        //}
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = new HttpClient();
            var response = await client.GetAsync($"{_baseUrl}/{id.Value}");
            if (response.IsSuccessStatusCode)
            {
                var inventory = JsonConvert.DeserializeObject<Inventory>(await response.Content.ReadAsStringAsync());
                return View(inventory);
            }
            return new HttpNotFoundResult();
        }

        // POST: Inventory/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Delete([Bind(Include = "Id,Timestamp")] Inventory inventory)
        //{
        //    try
        //    {
        //        _repo.Delete(inventory);
        //    }
        //    catch (DbUpdateConcurrencyException ex)
        //    {
        //        ModelState.AddModelError(string.Empty, $@"Unable to delete record. Another user updated the record. {ex.Message}");
        //    }
        //    catch (Exception ex)
        //    {
        //        ModelState.AddModelError(string.Empty, $@"Unable to create record: {ex.Message}");
        //    }

        //    return RedirectToAction("Index");
        //}
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete([Bind(Include = "Id,Timestamp")] Inventory inventory)
        {
            try
            {
                var client = new HttpClient();
                HttpRequestMessage request =
                   new HttpRequestMessage(HttpMethod.Delete, $"{_baseUrl}/{inventory.Id}")
                   {
                       Content =
                      new StringContent(JsonConvert.SerializeObject(inventory), Encoding.UTF8, "application/json")
                   };
                var response = await client.SendAsync(request);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, $"Unable to create record: {ex.Message}");
            }
            return View(inventory);
        }

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        _repo.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
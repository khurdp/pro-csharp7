﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncAwaitExample
{
    class Program
    {
        static void Main(string[] args)
        {
            var demo = new AsyncAwaitDemo();
            demo.DoStuff();

            for (var counter = 0; counter < 50; counter++)
            {
                Console.WriteLine($"{counter} Doing Stuff on the Main Thread...................");
                Thread.Sleep(100);
            }
            Console.ReadLine();
        }
    }

    public class AsyncAwaitDemo
    {
                public async Task DoStuff()
                {
                    await Task.Run(() =>
                    {
                        Thread.Sleep(1000);
                        var s = LongRunningOperation();
                    });
                }

                private static async Task<string> LongRunningOperation()
                {
                    int counter;

                    for (counter = 0; counter < 200; counter++)
                    {
                        Thread.Sleep(20);
                        Console.WriteLine(counter);
                    }

                    return "Counter = " + counter;
                } 

/*                public void DoStuff()
                { 
                        Thread.Sleep(1000);
                        var s = LongRunningOperation();
                }

                private static string LongRunningOperation()
                {
                    int counter;

                    for (counter = 0; counter < 200; counter++)
                    {
                        Console.WriteLine(counter);
                    }

                    return "Counter = " + counter;
                } */
    }
}

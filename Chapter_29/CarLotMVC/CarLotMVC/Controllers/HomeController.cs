﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CarLotMVC.Controllers.Filters;

namespace CarLotMVC.Controllers
{
    public class HomeController : Controller
    {
        [CustomResult]
        [CustomAuthentication]
        [CustomAuthorization]
        [CustomAction]
        [CustomException]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [OutputCache(Duration = 10)]
        public string OutputCacheTest()
        {
            return DateTime.Now.ToString("T");
        }
    }
}
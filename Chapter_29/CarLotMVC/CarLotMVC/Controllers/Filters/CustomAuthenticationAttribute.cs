﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Filters;

namespace CarLotMVC.Controllers.Filters
{
    public class CustomAuthenticationAttribute : ActionFilterAttribute, IAuthenticationFilter
    {
        public void OnAuthentication(AuthenticationContext filterContext)
        {
//            Debug.WriteLine("OnAuthentication");

            filterContext.Controller.ViewBag.OnAuthentication = "IAuthenticationFilter.OnAuthentication filter called";

            if (!filterContext.Principal.Identity.IsAuthenticated)
            {
            filterContext.Controller.ViewBag.OnAuthentication += "\nIsAuthenticated...";

/*                filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary(
                        new { controller = "Home", action = "Login" })); */
            }
        }

        public void OnAuthenticationChallenge(AuthenticationChallengeContext filterContext)
        {
            filterContext.Controller.ViewBag.OnAuthenticated = "OnAuthenticationChallenge called";

            //            Debug.WriteLine("OnAuthenticationChallenge");
        }
    }
}
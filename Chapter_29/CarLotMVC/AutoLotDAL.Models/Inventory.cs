using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AutoLotDAL.Models.Base;

namespace AutoLotDAL.Models
{
    [Table("Inventory")]
    public partial class Inventory : EntityBase
    {
        public Inventory()
        {
            Orders = new HashSet<Order>();
        }

        [Key]
        public int Id { get; set; }

        [StringLength(50)]
        public string Make { get; set; }

        [StringLength(50)]
        public string Color { get; set; }

        [StringLength(50), Column("PetName")]
        public string PetName { get; set; }

        public virtual ICollection<Order> Orders { get; set; } 
            = new HashSet<Order>();
    }
}
